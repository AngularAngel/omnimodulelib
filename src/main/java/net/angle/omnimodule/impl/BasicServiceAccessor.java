package net.angle.omnimodule.impl;

import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnimodule.api.ServiceAccessor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicServiceAccessor<T> implements ServiceAccessor<T> {
    
    private BundleContext context;
    private ServiceReference<T> serviceReference;
    private @Getter T service;
    private final Class<T> serviceClass;
    
    @Override
    public void beginUsingService(T service) {}

    @Override
    public void start(BundleContext context) throws Exception {
        this.context = context;
        serviceReference = context.getServiceReference(serviceClass);
        if (serviceReference != null)
            service = context.getService(serviceReference);
        if (hasAcquiredService())
            beginUsingService(service);
        try {
            context.addServiceListener(this, "(objectclass=" + serviceClass.getName() + ")");
        } catch (InvalidSyntaxException ise) {
            ise.printStackTrace();
        }
    }
    
    @Override
    public boolean hasAcquiredService() {
        return Objects.nonNull(service);
    }

    @Override
    public void serviceChanged(ServiceEvent serviceEvent) {
        int type = serviceEvent.getType();
        switch (type){
            case(ServiceEvent.REGISTERED) -> {
                serviceReference = (ServiceReference<T>) serviceEvent
                        .getServiceReference();
                service = context.getService(serviceReference);
                if (hasAcquiredService())
                    beginUsingService(service);
            }
            case(ServiceEvent.UNREGISTERING) -> {
                context.ungetService(serviceEvent.getServiceReference());
                serviceReference = null;
                service = null;
            }
            default -> {}
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if(serviceReference != null) {
            context.ungetService(serviceReference);
        }
    }
}