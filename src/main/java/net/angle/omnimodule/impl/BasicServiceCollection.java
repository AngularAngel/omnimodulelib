package net.angle.omnimodule.impl;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import net.angle.omnimodule.api.ServiceAccessor;
import net.angle.omnimodule.api.ServiceCollection;
import org.osgi.framework.BundleContext;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicServiceCollection implements ServiceCollection {
    private final List<ServiceAccessor> serviceAccessors =  new ArrayList<>();
    
    public final Runnable finalAction;
    
    public boolean serviceAccessorsReady() {
        for (ServiceAccessor serviceAccessor : serviceAccessors)
            if (!serviceAccessor.hasAcquiredService())
                return false;
        return true;
    }
    
    public void serviceAccessed() {
        if (serviceAccessorsReady())
            finalAction.run();
    }
    
    @Override
    public <T> ServiceAccessor<T> addService(Class<T> interfaceClass) {
        ServiceAccessor<T> serviceAccessor = new BasicServiceAccessor<>(interfaceClass) {
            @Override
            public void beginUsingService(T service) {
                serviceAccessed();
            }
        };
        serviceAccessors.add(serviceAccessor);
        return serviceAccessor;
    }
    
    @Override
    public void start(BundleContext context) throws Exception {
        for (ServiceAccessor serviceAccessor : serviceAccessors)
            serviceAccessor.start(context);
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        for (ServiceAccessor serviceAccessor : serviceAccessors)
            serviceAccessor.stop(context);
    }
}