package net.angle.omnimodule.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnimodule.api.OmniModule;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public abstract class BasicOmniModule implements OmniModule {
    
    private final @Getter Class interfaceClass;
}
