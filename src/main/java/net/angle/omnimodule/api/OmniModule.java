package net.angle.omnimodule.api;

import org.osgi.framework.BundleActivator;

/**
 *
 * @author angle
 */
public interface OmniModule {
    public Class<? extends OmniModule> getInterfaceClass();
}