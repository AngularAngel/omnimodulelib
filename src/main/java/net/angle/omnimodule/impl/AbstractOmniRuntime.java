package net.angle.omnimodule.impl;

import java.util.HashMap;
import java.util.Map;
import net.angle.omnimodule.api.OmniModule;
import net.angle.omnimodule.api.OmniRuntime;
import net.angle.omnimodule.api.ServiceAccessor;
import net.angle.omnimodule.api.ServiceCollection;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
/**
 *
 * @author angle
 */
public abstract class AbstractOmniRuntime implements BundleActivator, OmniRuntime {
    private final ServiceCollection serviceCollection = new BasicServiceCollection(this::createAndStartRuntime);
    
    private final Map<Class<? extends OmniModule>, ServiceAccessor<? extends OmniModule>> serviceAccessors = new HashMap<>();
    
    public <T extends OmniModule> T getService(Class<T> serviceclass) {
        return (T) serviceAccessors.get(serviceclass).getService();
    }
    
    public <T extends OmniModule> ServiceAccessor<T> addService(Class<T> serviceclass) {
        ServiceAccessor<T> serviceAccessor = serviceCollection.addService(serviceclass);
        serviceAccessors.put(serviceclass, serviceAccessor);
        return serviceAccessor;
    }
    
    public abstract void createAndStartRuntime();
    
    @Override
    public void start(BundleContext context) throws Exception {
        serviceCollection.start(context);
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceCollection.stop(context);
    }
}