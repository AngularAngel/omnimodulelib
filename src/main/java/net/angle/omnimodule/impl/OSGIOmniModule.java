package net.angle.omnimodule.impl;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 *
 * @author angle
 */
public abstract class OSGIOmniModule extends BasicOmniModule implements BundleActivator  {
    private ServiceRegistration<OSGIOmniModule> registration = null;

    public OSGIOmniModule(Class interfaceClass) {
        super(interfaceClass);
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public void start(BundleContext context) throws Exception {
        registration = context.registerService(
          getInterfaceClass(), 
          this, 
          new Hashtable<String, String>());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        registration.unregister();
    }
}