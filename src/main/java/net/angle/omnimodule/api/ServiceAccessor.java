package net.angle.omnimodule.api;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

/**
 *
 * @author angle
 */
public interface ServiceAccessor<T> extends ServiceListener {
    public T getService();
            
    public void start(BundleContext context) throws Exception;
    public void stop(BundleContext context) throws Exception;
    
    public void beginUsingService(T service);
    public boolean hasAcquiredService();

    @Override
    public void serviceChanged(ServiceEvent serviceEvent);
}