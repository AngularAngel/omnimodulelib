package net.angle.omnimodule.api;

import org.osgi.framework.BundleActivator;

/**
 *
 * @author angle
 */
public interface OmniRuntime extends BundleActivator {
    
    public <T extends OmniModule> T getService(Class<T> serviceclass);
    
    public <T extends OmniModule> ServiceAccessor<T> addService(Class<T> serviceclass);
    
    public void createAndStartRuntime();
}