package net.angle.omnimodule.api;

import org.osgi.framework.BundleContext;

/**
 *
 * @author angle
 */
public interface ServiceCollection {
    public <T> ServiceAccessor<T> addService(Class<T> interfaceClass);
    
    public void start(BundleContext context) throws Exception;
    
    public void stop(BundleContext context) throws Exception;
}