
module omni.module {
    requires static lombok;
    requires osgi.core;
    exports net.angle.omnimodule.impl;
    exports net.angle.omnimodule.api;
}
